package ro.treedev.personalitytest.application

import android.app.Application
import ro.treedev.personalitytest.questions.dal.QuestionModule
import ro.treedev.personalitytest.result.dal.ResultCalculatorModule
import toothpick.ktp.KTP
import toothpick.smoothie.module.SmoothieApplicationModule

@Suppress("Is used directly from Manifest.xml but the IDE can't see it", "unused")
class PersonalityTestApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        KTP.openScope(this).installModules(
            SmoothieApplicationModule(this),
            QuestionModule(),
            ResultCalculatorModule()
        ).inject(this)
    }
}