package ro.treedev.personalitytest.result.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import ro.treedev.personalitytest.R
import ro.treedev.personalitytest.util.ToothpickViewModelFactory
import toothpick.ktp.KTP
import toothpick.smoothie.lifecycle.closeOnDestroy
import javax.inject.Inject

class ResultFragment : Fragment() {

    companion object {
        const val KEY_RESULT_SCORE = "result_score"
    }

    @Inject
    lateinit var toothpickViewModelFactory: ToothpickViewModelFactory

    private var title: TextView? = null
    private var description: TextView? = null

    private val viewModel: ResultViewModel by lazy {
        ViewModelProvider(this, toothpickViewModelFactory).get(ResultViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        KTP.openScopes(requireActivity().application, requireActivity(), this)
            .closeOnDestroy(this)
            .inject(this)
        (requireActivity() as AppCompatActivity).supportActionBar?.title =
            getString(R.string.result)
        val score = arguments?.getInt(KEY_RESULT_SCORE) ?: -1
        title = view.findViewById(R.id.title)
        description = view.findViewById(R.id.description)

        viewModel.title.observe(viewLifecycleOwner, {
            title?.text = it
        })

        viewModel.description.observe(viewLifecycleOwner, {
            description?.text = it
        })

        viewModel.loadScore(score)
    }

}