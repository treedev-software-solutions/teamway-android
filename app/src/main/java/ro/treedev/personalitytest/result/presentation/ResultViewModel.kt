package ro.treedev.personalitytest.result.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ro.treedev.personalitytest.result.LoadResultUseCase
import toothpick.InjectConstructor

@InjectConstructor
class ResultViewModel(
    private val loadResultUseCase: LoadResultUseCase
) : ViewModel() {

    private val _title: MutableLiveData<String> = MutableLiveData()
    val title: LiveData<String>
        get() = _title

    private val _description: MutableLiveData<String> = MutableLiveData()
    val description: LiveData<String>
        get() = _description

    fun loadScore(score: Int) {
        viewModelScope.launch {
            val resultScore = loadResultUseCase(score)
            _title.value = resultScore?.title
            _description.value = resultScore?.description
        }
    }

}