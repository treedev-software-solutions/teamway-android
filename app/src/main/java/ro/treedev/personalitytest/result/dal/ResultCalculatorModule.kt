package ro.treedev.personalitytest.result.dal

import ro.treedev.personalitytest.result.repository.ResultCalculatorRepository
import toothpick.config.Module

class ResultCalculatorModule : Module() {
    init {
        bind(ResultCalculatorRepository::class.java).to(ResultCalculatorRepositoryImpl::class.java)
    }
}