package ro.treedev.personalitytest.result.dal

import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import ro.treedev.personalitytest.result.repository.ResultCalculatorRepository
import ro.treedev.personalitytest.result.repository.model.ResultScore
import toothpick.InjectConstructor
import java.util.*

@InjectConstructor
class ResultCalculatorRepositoryImpl(
    private val usScoreCalculator: USScoreCalculator
) : ResultCalculatorRepository {

    override suspend fun getResultByScore(score: Int, locale: Locale): ResultScore? =
        withContext(IO) {
            return@withContext when (locale) {
                Locale.US -> usScoreCalculator.getResultByScore(score)
                else -> null
            }
        }
}