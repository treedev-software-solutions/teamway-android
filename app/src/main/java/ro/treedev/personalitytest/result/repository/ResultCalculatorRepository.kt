package ro.treedev.personalitytest.result.repository

import ro.treedev.personalitytest.result.repository.model.ResultScore
import java.util.*

interface ResultCalculatorRepository {

    suspend fun getResultByScore(score: Int, locale: Locale = Locale.US): ResultScore?

}