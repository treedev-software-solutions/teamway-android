package ro.treedev.personalitytest.result.repository.model

data class ResultScore(
    val title: String,
    val description: String
)
