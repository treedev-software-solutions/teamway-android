package ro.treedev.personalitytest.result

import ro.treedev.personalitytest.result.repository.ResultCalculatorRepository
import ro.treedev.personalitytest.result.repository.model.ResultScore
import toothpick.InjectConstructor

@InjectConstructor
class LoadResultUseCase(private val resultCalculatorRepository: ResultCalculatorRepository) {

    suspend operator fun invoke(score: Int) : ResultScore? = resultCalculatorRepository.getResultByScore(score)

}