package ro.treedev.personalitytest.questions.repository

import ro.treedev.personalitytest.questions.repository.model.Question
import java.util.*

interface QuestionRepository {

    suspend fun getNextQuestion(questionNr: Int = 0, locale: Locale = Locale.US) : Question?

    suspend fun getQuestionCount() : Int
}