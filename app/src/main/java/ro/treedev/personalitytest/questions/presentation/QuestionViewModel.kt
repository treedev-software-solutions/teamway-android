package ro.treedev.personalitytest.questions.presentation

import androidx.lifecycle.*
import com.hadilq.liveevent.LiveEvent
import kotlinx.coroutines.launch
import ro.treedev.personalitytest.R
import ro.treedev.personalitytest.questions.GetNextQuestionUseCase
import ro.treedev.personalitytest.questions.GetQuestionCountUseCase
import ro.treedev.personalitytest.questions.presentation.model.QuestionModel
import ro.treedev.personalitytest.questions.repository.model.Question
import ro.treedev.personalitytest.util.StringProvider
import toothpick.InjectConstructor

@InjectConstructor
class QuestionViewModel(
    private val getQuestionCountUseCase: GetQuestionCountUseCase,
    private val getNextQuestionUseCase: GetNextQuestionUseCase,
    private val stringProvider: StringProvider
) : ViewModel() {

    //key = question nr
    //value = points selected
    private val answers: MutableLiveData<HashMap<Int, Int>> = MutableLiveData(hashMapOf())

    private var currentQuestionNumber = -1
    private var questionCount: Int = -1

    private val _progressPercentage: MutableLiveData<Pair<Int, Int>> = MutableLiveData(Pair(0, 0))
    val progressPercentage: LiveData<Pair<Int, Int>>
        get() = _progressPercentage

    private val questionSource: MutableLiveData<Question> = MutableLiveData()

    val currentQuestion: MediatorLiveData<QuestionModel> = MediatorLiveData()

    private val _showResult: LiveEvent<Int> = LiveEvent()
    val showResult: LiveData<Int>
        get() = _showResult

    private val _title: MutableLiveData<String> = MutableLiveData()
    val title: LiveData<String>
        get() = _title

    private val _isPreviousEnabled: MutableLiveData<Boolean> = MutableLiveData()
    val isPreviousEnabled: LiveData<Boolean>
        get() = _isPreviousEnabled

    private val _nextButtonText: MutableLiveData<String> = MutableLiveData()
    val nextButtonText: LiveData<String>
        get() = _nextButtonText

    init {
        viewModelScope.launch {
            questionCount = getQuestionCountUseCase()
            getNextQuestion()
        }

        currentQuestion.addSource(answers) {
            currentQuestion.value = transform(questionSource.value, it)
        }
        currentQuestion.addSource(questionSource) {
            currentQuestion.value = transform(it, answers.value!!)
        }
    }

    private suspend fun getNextQuestion() {
        currentQuestionNumber++
        questionSource.value = getNextQuestionUseCase(currentQuestionNumber)
        calculateProgressPercentage()
        setIsPreviousEnabled()
        setNextButtonText()
    }


    private fun setIsPreviousEnabled() {
        _isPreviousEnabled.value = currentQuestionNumber != 0
    }

    private fun setNextButtonText() {
        _nextButtonText.value = stringProvider.getString(
            if (currentQuestionNumber + 1 == questionCount)
                R.string.finish
            else
                R.string.next
        )
    }

    private suspend fun getPreviousQuestion() {
        currentQuestionNumber--
        questionSource.value = getNextQuestionUseCase(currentQuestionNumber)
        calculateProgressPercentage()
        setIsPreviousEnabled()
        setNextButtonText()
    }

    private fun calculateProgressPercentage() {
        _progressPercentage.value = Pair(currentQuestionNumber, questionCount)
        _title.value = stringProvider.getString(R.string.question_out_of, (currentQuestionNumber + 1), questionCount)
    }

    private fun transform(question: Question?, answers: HashMap<Int, Int>): QuestionModel? =
        if (question != null) {
            QuestionModel(
                question,
                answers[currentQuestionNumber] ?: -1
            )
        } else {
            null
        }


    fun onNextClicked() {
        viewModelScope.launch {
            if (currentQuestionNumber < questionCount - 1) {
                getNextQuestion()
            } else {
                var responseCount = 0
                answers.value!!.forEach { mapEntry ->
                    responseCount += mapEntry.value
                }
                _showResult.value = responseCount
            }
        }
    }

    fun onPreviousClicked() {
        viewModelScope.launch {
            getPreviousQuestion()
        }
    }

    fun onBackPressedHandled(): Boolean {
        return if (currentQuestionNumber != 0) {
            onPreviousClicked()
            true
        } else {
            false
        }
    }

    fun onAnswerSelected(second: Int) {
        val hashMap = answers.value!!
        hashMap[currentQuestionNumber] = second
        answers.value = hashMap
    }

}