package ro.treedev.personalitytest.questions

import ro.treedev.personalitytest.questions.repository.QuestionRepository
import ro.treedev.personalitytest.questions.repository.model.Question
import toothpick.InjectConstructor

@InjectConstructor
class GetNextQuestionUseCase(private val questionRepository: QuestionRepository) {

    suspend operator fun invoke(questionNumber: Int) : Question? = questionRepository.getNextQuestion(questionNumber)

}