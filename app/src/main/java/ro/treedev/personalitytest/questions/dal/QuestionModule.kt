package ro.treedev.personalitytest.questions.dal

import ro.treedev.personalitytest.questions.repository.QuestionRepository
import toothpick.config.Module

class QuestionModule : Module() {

    init {
        bind(QuestionRepository::class.java).to(LocalQuestionRepository::class.java)
    }
}