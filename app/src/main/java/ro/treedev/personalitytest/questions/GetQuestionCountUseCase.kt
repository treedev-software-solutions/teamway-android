package ro.treedev.personalitytest.questions

import ro.treedev.personalitytest.questions.repository.QuestionRepository
import toothpick.InjectConstructor

@InjectConstructor
class GetQuestionCountUseCase(private val questionRepository: QuestionRepository) {

    suspend operator fun invoke() : Int = questionRepository.getQuestionCount()

}