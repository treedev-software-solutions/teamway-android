package ro.treedev.personalitytest.questions.dal

import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import ro.treedev.personalitytest.questions.repository.QuestionRepository
import ro.treedev.personalitytest.questions.repository.model.Question
import toothpick.InjectConstructor
import java.util.*

@InjectConstructor
class LocalQuestionRepository(private val usQuestionHandler: USQuestionHandler) :
    QuestionRepository {

    override suspend fun getNextQuestion(questionNr: Int, locale: Locale): Question? =
        withContext(IO) {
            return@withContext when (locale) {
                Locale.US -> usQuestionHandler.getNextQuestion(questionNr)
                else -> null
            }
        }

    override suspend fun getQuestionCount(): Int {
        return 14
    }

}