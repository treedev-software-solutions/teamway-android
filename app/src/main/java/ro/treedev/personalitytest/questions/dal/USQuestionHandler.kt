package ro.treedev.personalitytest.questions.dal

import ro.treedev.personalitytest.questions.repository.model.Question
import toothpick.InjectConstructor

@InjectConstructor
class USQuestionHandler {

    fun getNextQuestion(questionNr: Int): Question? {
        return when (questionNr) {
            0 -> Question(
                "You’re really busy at work and a colleague is telling you their life story and personal woes. You:",
                arrayListOf(
                    Pair("Don’t dare to interrupt them", 1),
                    Pair("Think it’s more important to give them some of your time; work can wait", 2),
                    Pair("Listen, but with only with half an ear", 3),
                    Pair("Interrupt and explain that you are really busy at the moment", 4)
                )
            )
            1 -> Question(
                "You’ve been sitting in the doctor’s waiting room for more than 25 minutes. You:",
                arrayListOf(
                    Pair("Look at your watch every two minutes", 1),
                    Pair("Bubble with inner anger, but keep quiet", 2),
                    Pair("Explain to other equally impatient people in the room that the doctor is always running late", 3),
                    Pair("Complain in a loud voice, while tapping your foot impatiently", 4)
                )
            )
            2 -> Question(
                "You’re having an animated discussion with a colleague regarding a project that you’re in charge of. You:",
                arrayListOf(
                    Pair("Don’t dare contradict them", 1),
                    Pair("Think that they are obviously right", 2),
                    Pair("Defend your own point of view, tooth and nail", 3),
                    Pair("Continuously interrupt your colleague", 4)
                )
            )
            3 -> Question(
                "You are taking part in a guided tour of a museum. You:",
                arrayListOf(
                    Pair("Are a bit too far towards the back so don’t really hear what the guide is saying", 1),
                    Pair("Follow the group without question", 2),
                    Pair("Make sure that everyone is able to hear properly", 3),
                    Pair("Are right up the front, adding your own comments in a loud voice", 4)
                )
            )
            4 -> Question(
                "During dinner parties at your home, you have a hard time with people who:",
                arrayListOf(
                    Pair("Ask you to tell a story in front of everyone else", 1),
                    Pair("Talk privately between themselves", 2),
                    Pair("Hang around you all evening", 3),
                    Pair("Always drag the conversation back to themselves", 4)
                )
            )
            5 -> Question(
                "You crack a joke at work, but nobody seems to have noticed. You:",
                arrayListOf(
                    Pair("Think it’s for the best — it was a lame joke anyway", 1),
                    Pair("Wait to share it with your friends after work", 2),
                    Pair("Try again a bit later with one of your colleagues", 3),
                    Pair("Keep telling it until they pay attention", 4)
                )
            )
            6 -> Question(
                "This morning, your agenda seems to be free. You:",
                arrayListOf(
                    Pair("Know that somebody will find a reason to come and bother you", 1),
                    Pair("Heave a sigh of relief and look forward to a day without stress", 2),
                    Pair("Question your colleagues about a project that’s been worrying you", 3),
                    Pair("Pick up the phone and start filling up your agenda with meetings", 4)
                )
            )
            7 -> Question(
                "During dinner, the discussion moves to a subject about which you know nothing at all. You:",
                arrayListOf(
                    Pair("Don’t dare show that you don’t know anything about the subject", 1),
                    Pair("Barely follow the discussion", 2),
                    Pair("Ask lots of questions to learn more about it", 3),
                    Pair("Change the subject of discussion", 4)
                )
            )
            8 -> Question(
                "You’re out with a group of friends and there’s a person who’s quite shy and doesn’t talk much. You:",
                arrayListOf(
                    Pair("Notice that they’re alone, but don’t go over to talk with them", 1),
                    Pair("Go and have a chat with them", 2),
                    Pair("Shoot some friendly smiles in their direction", 3),
                    Pair("Hardly notice them at all", 4)
                )
            )
            9 -> Question(
                "At work, somebody asks for your help for the hundredth time. You:",
                arrayListOf(
                    Pair("Give them a hand, as usual", 1),
                    Pair("Accept — you’re known for being helpful", 2),
                    Pair("Ask them, please, to find somebody else for a change", 3),
                    Pair("Loudly make it known that you’re annoyed", 4)
                )
            )
            10 -> Question(
                "You’ve been see a movie with your family and the reviews are mixed. You:",
                arrayListOf(
                    Pair("Don’t share your point of view with anyone", 1),
                    Pair("Didn’t like the film, but keep your views to yourself when asked", 2),
                    Pair("State your point of view with enthusiasm", 3),
                    Pair("Try to bring the others round to your point of view", 4)
                )
            )
            11-> Question(
                "A friend arrives late for your meeting. You:",
                arrayListOf(
                    Pair("Say, ‘It’s not a problem,’ even if that’s not what you really think", 1),
                    Pair("Give them a filthy look and sulk for the rest of the evening", 2),
                    Pair("Tell them, ‘You’re too much! Have you seen the time?’", 3),
                    Pair("Make a scene in front of everyone", 4)
                )
            )
            12-> Question(
                "You can’t find your car keys. You:",
                arrayListOf(
                    Pair("Don’t want anyone to find out, so you take the bus instead", 1),
                    Pair("Panic and search madly without asking anyone for help", 2),
                    Pair("Grumble without telling your family why you’re in a bad mood", 3),
                    Pair("Accuse those around you for misplacing them", 4)
                )
            )
            13-> Question(
                "It’s time for your annual appraisal with your boss. You:",
                arrayListOf(
                    Pair("Go with great hesitation as these sessions are torture for you", 1),
                    Pair("Look forward to hearing what your boss thinks about you and expects from you", 2),
                    Pair("Rehearse ad nauseam the arguments and ideas that you’ve prepared for the meeting", 3),
                    Pair("Go along unprepared as you are confident and like improvising", 4)
                )
            )
            else -> null
        }
    }

}