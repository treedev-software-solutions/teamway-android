package ro.treedev.personalitytest.questions.presentation.model

import ro.treedev.personalitytest.questions.repository.model.Question

data class QuestionModel(
    val question: Question,
    val selected: Int
)