package ro.treedev.personalitytest.questions.presentation

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.navigation.fragment.findNavController
import ro.treedev.personalitytest.R
import ro.treedev.personalitytest.result.presentation.ResultFragment
import ro.treedev.personalitytest.util.ToothpickViewModelFactory
import toothpick.ktp.KTP
import toothpick.smoothie.lifecycle.closeOnDestroy
import javax.inject.Inject

class QuestionFragment : Fragment(), View.OnClickListener {

    @Inject
    lateinit var toothpickViewModelFactory: ToothpickViewModelFactory
    private lateinit var title: TextView
    private lateinit var optionsRadioGroup: RadioGroup
    private lateinit var next: AppCompatButton
    private lateinit var previous: AppCompatButton
    private lateinit var questionProgress: ProgressBar

    private val viewModel: QuestionViewModel by lazy {
        ViewModelProvider(this, toothpickViewModelFactory).get(QuestionViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_question, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        KTP.openScopes(requireActivity().application, requireActivity(), this)
            .closeOnDestroy(this)
            .inject(this)

        optionsRadioGroup = view.findViewById(R.id.options)
        title = view.findViewById(R.id.title)
        next = view.findViewById(R.id.next_button)
        previous = view.findViewById(R.id.previous_button)
        questionProgress = view.findViewById(R.id.question_progress)

        next.setOnClickListener(this)
        previous.setOnClickListener(this)

        viewModel.currentQuestion.observe(viewLifecycleOwner, { questionModel ->
            optionsRadioGroup.removeAllViews()
            if (questionModel != null) {
                title.text = questionModel.question.question
                questionModel.question.options.forEach { option ->
                    val radioButton = RadioButton(requireContext())
                    radioButton.text = option.first
                    radioButton.setOnClickListener {
                        viewModel.onAnswerSelected(option.second)
                    }
                    if (option.second == questionModel.selected) {
                        radioButton.isChecked = true
                    }
                    optionsRadioGroup.addView(
                        radioButton
                    )
                }
            }
        })

        viewModel.progressPercentage.observe(viewLifecycleOwner, { percentage->
            questionProgress.max = percentage.second
            questionProgress.progress = percentage.first
        })

        viewModel.title.observe(viewLifecycleOwner, { title->
            (requireActivity() as AppCompatActivity).supportActionBar?.title = title
        })

        viewModel.isPreviousEnabled.observe(viewLifecycleOwner, { isEnabled->
            previous.isEnabled = isEnabled
        })

        viewModel.nextButtonText.observe(viewLifecycleOwner, {
            next.text = it
        })

        viewModel.showResult.observe(viewLifecycleOwner, { resultScore->
            val bundle = Bundle()
            bundle.putInt(ResultFragment.KEY_RESULT_SCORE, resultScore)
            findNavController().navigate(R.id.action_question_to_resultFragment, bundle)
        })

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (!viewModel.onBackPressedHandled()) {
                    findNavController().popBackStack()
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.next_button -> viewModel.onNextClicked()
            R.id.previous_button -> viewModel.onPreviousClicked()
        }
    }

}