package ro.treedev.personalitytest.questions.repository.model

data class Question(
    val question: String,
    val options: List<Pair<String, Int>>
)