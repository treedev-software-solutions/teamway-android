package ro.treedev.personalitytest.main.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import ro.treedev.personalitytest.R

class MainFragment : Fragment(), View.OnClickListener {

    private var startButton: Button? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startButton = view.findViewById(R.id.start)
        startButton?.setOnClickListener(this)
        (requireActivity() as AppCompatActivity).supportActionBar?.title = getString(R.string.app_name)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.start -> {
                findNavController().navigate(R.id.question)
            }
        }
    }

    override fun onDestroy() {
        startButton?.setOnClickListener(null)
        super.onDestroy()
    }
}