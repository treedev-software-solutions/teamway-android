package ro.treedev.personalitytest.main.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ro.treedev.personalitytest.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}