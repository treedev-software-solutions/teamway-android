package ro.treedev.personalitytest.util

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import toothpick.Toothpick
import javax.inject.Inject

class ToothpickViewModelFactory @Inject constructor(private val application: Application):  ViewModelProvider.Factory  {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return Toothpick.openScope(application).getInstance(modelClass)
    }

}