package ro.treedev.personalitytest.util

import android.app.Application
import androidx.annotation.StringRes
import javax.inject.Inject

class StringProvider @Inject constructor(private val application: Application) {

    fun getString(@StringRes resId: Int): String {
        return application.getString(resId)
    }

    fun getString(@StringRes resId: Int, vararg formatArgs: Any?): String {
        return application.getString(resId, *formatArgs)
    }

}