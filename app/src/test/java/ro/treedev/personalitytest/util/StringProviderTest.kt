package ro.treedev.personalitytest.util

import android.app.Application
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import toothpick.testing.ToothPickRule
import javax.inject.Inject


internal class StringProviderTest {

    @Rule
    @JvmField
    var mockitoRule: MockitoRule = MockitoJUnit.rule()

    @Rule
    @JvmField
    var toothPickRule: ToothPickRule = ToothPickRule(this, "Test Scope")

    @Mock
    lateinit var application: Application

    @Inject
    lateinit var stringProvider: StringProvider

    @Before
    @Throws(Exception::class)
    fun setUp() {
        toothPickRule.inject(this)
    }

    @Test
    fun `getString returns TEST when id is 1`() {
        //Given
        val string = "TEST"
        whenever(application.getString(1)).thenReturn(string)
        //When
        val result = stringProvider.getString(1)
        //then
        assertEquals(string, result)
    }

    @Test
    fun `getString returns TEST when id is 1 and params are set`() {
        //Given
        val string = "TEST"
        whenever(application.getString(1, 1)).thenReturn(string)
        //When
        val result = stringProvider.getString(1, 1)
        //Then
        assertEquals(string, result)
    }
}