package ro.treedev.personalitytest.result.dal

import com.nhaarman.mockitokotlin2.whenever
import io.mockk.every
import io.mockk.mockkStatic
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.Assertions.*
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import ro.treedev.personalitytest.result.repository.model.ResultScore
import toothpick.testing.ToothPickRule
import java.util.*
import javax.inject.Inject

internal class ResultCalculatorRepositoryImplTest {
    @Rule
    @JvmField
    var mockitoRule: MockitoRule = MockitoJUnit.rule()

    @Rule
    @JvmField
    var toothPickRule: ToothPickRule = ToothPickRule(this, "Test Scope")


    @Mock
    lateinit var usScoreCalculator: USScoreCalculator

    @Inject
    lateinit var resultCalculatorRepositoryImpl: ResultCalculatorRepositoryImpl

    @Before
    @Throws(Exception::class)
    fun setUp() {
        toothPickRule.inject(this)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `verifies if the US calculator is called`() = runBlockingTest {
        //Given
        mockkStatic(Dispatchers::class)
        every { Dispatchers.IO } returns TestCoroutineDispatcher()
        val mockedLocale = Locale.US
        val score = 14
        val mockResult = ResultScore("test", "test")
        whenever(usScoreCalculator.getResultByScore(score)).thenReturn(mockResult)
        //When
        val result = resultCalculatorRepositoryImpl.getResultByScore(score, mockedLocale)
        //Then
        assertEquals(mockResult, result)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `verifies if not the US calculator is called`() = runBlockingTest {
        //Given
        mockkStatic(Dispatchers::class)
        every { Dispatchers.IO } returns TestCoroutineDispatcher()
        val mockedLocale = Locale.GERMAN
        val score = 14
        //When
        val result = resultCalculatorRepositoryImpl.getResultByScore(score, mockedLocale)
        //Then
        assertNull(result)
    }
}