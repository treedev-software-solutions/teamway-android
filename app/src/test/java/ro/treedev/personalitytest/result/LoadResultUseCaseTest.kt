package ro.treedev.personalitytest.result

import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import ro.treedev.personalitytest.result.repository.ResultCalculatorRepository
import ro.treedev.personalitytest.result.repository.model.ResultScore
import toothpick.testing.ToothPickRule
import java.util.*
import javax.inject.Inject

internal class LoadResultUseCaseTest {

    @Rule
    @JvmField
    var mockitoRule: MockitoRule = MockitoJUnit.rule()

    @Rule
    @JvmField
    var toothPickRule: ToothPickRule = ToothPickRule(this, "Test Scope")

    @Mock
    lateinit var resultCalculatorRepository: ResultCalculatorRepository

    @Inject
    lateinit var loadResultUseCase: LoadResultUseCase

    @Before
    @Throws(Exception::class)
    fun setUp() {
        toothPickRule.inject(this)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `invoke calls repository`() = runBlockingTest {
        //Given
        val mockResultScore = 2
        val mockedResult = ResultScore("title","description")
        whenever(resultCalculatorRepository.getResultByScore(mockResultScore, Locale.US)).thenReturn(
            mockedResult
        )
        //When
        val result = loadResultUseCase(mockResultScore)
        //Then
        assertEquals(mockedResult, result)
    }

}