package ro.treedev.personalitytest

import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.IsInstanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ro.treedev.personalitytest.main.presentation.MainActivity

@LargeTest
@RunWith(AndroidJUnit4::class)
class HappyFlowExtrovert {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun happyFlowExtrovert() {
        val button = onView(
            allOf(
                withId(R.id.start), withText("START"),
                withParent(
                    allOf(
                        withId(R.id.main),
                        withParent(withId(R.id.my_nav_host_fragment))
                    )
                ),
                isDisplayed()
            )
        )
        button.check(matches(isDisplayed()))

        val materialButton = onView(
            allOf(
                withId(R.id.start), withText("Start"),
                childAtPosition(
                    allOf(
                        withId(R.id.main),
                        childAtPosition(
                            withId(R.id.my_nav_host_fragment),
                            0
                        )
                    ),
                    5
                ),
                isDisplayed()
            )
        )
        materialButton.perform(click())

        val textView = onView(
            allOf(
                withText("Question 1 of 14"),
                withParent(
                    allOf(
                        withId(R.id.action_bar),
                        withParent(withId(R.id.action_bar_container))
                    )
                ),
                isDisplayed()
            )
        )
        textView.check(matches(withText("Question 1 of 14")))

        val textView2 = onView(
            allOf(
                withId(R.id.title),
                withText("You’re really busy at work and a colleague is telling you their life story and personal woes. You:"),
                withParent(withParent(withId(R.id.my_nav_host_fragment))),
                isDisplayed()
            )
        )
        textView2.check(matches(withText("You’re really busy at work and a colleague is telling you their life story and personal woes. You:")))

        val radioButton = onView(
            allOf(
                withText("Don’t dare to interrupt them"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton.check(matches(isDisplayed()))

        val radioButton2 = onView(
            allOf(
                withText("Think it’s more important to give them some of your time; work can wait"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton2.check(matches(isDisplayed()))

        val radioButton3 = onView(
            allOf(
                withText("Listen, but with only with half an ear"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton3.check(matches(isDisplayed()))

        val radioButton4 = onView(
            allOf(
                withText("Interrupt and explain that you are really busy at the moment"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton4.check(matches(isDisplayed()))

        val button2 = onView(
            allOf(
                withId(R.id.previous_button), withText("PREVIOUS"),
                withParent(withParent(withId(R.id.cardView))),
                isDisplayed()
            )
        )
        button2.check(matches(isDisplayed()))

        val button3 = onView(
            allOf(
                withId(R.id.next_button), withText("NEXT"),
                withParent(withParent(withId(R.id.cardView))),
                isDisplayed()
            )
        )
        button3.check(matches(isDisplayed()))

        val button4 = onView(
            allOf(
                withId(R.id.next_button), withText("NEXT"),
                withParent(withParent(withId(R.id.cardView))),
                isDisplayed()
            )
        )
        button4.check(matches(isDisplayed()))

        val appCompatButton = onView(
            allOf(
                withId(R.id.next_button), withText("Next"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.cardView),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        appCompatButton.perform(click())

        val textView3 = onView(
            allOf(
                withText("Question 2 of 14"),
                withParent(
                    allOf(
                        withId(R.id.action_bar),
                        withParent(withId(R.id.action_bar_container))
                    )
                ),
                isDisplayed()
            )
        )
        textView3.check(matches(withText("Question 2 of 14")))

        val textView4 = onView(
            allOf(
                withId(R.id.title),
                withText("You’ve been sitting in the doctor’s waiting room for more than 25 minutes. You:"),
                withParent(withParent(withId(R.id.my_nav_host_fragment))),
                isDisplayed()
            )
        )
        textView4.check(matches(withText("You’ve been sitting in the doctor’s waiting room for more than 25 minutes. You:")))

        val radioGroup = onView(
            allOf(
                withId(R.id.options),
                withParent(withParent(withId(R.id.my_nav_host_fragment))),
                isDisplayed()
            )
        )
        radioGroup.check(matches(isDisplayed()))

        val radioButton5 = onView(
            allOf(
                withText("Look at your watch every two minutes"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton5.check(matches(isDisplayed()))

        val radioButton6 = onView(
            allOf(
                withText("Bubble with inner anger, but keep quiet"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton6.check(matches(isDisplayed()))

        val radioButton7 = onView(
            allOf(
                withText("Explain to other equally impatient people in the room that the doctor is always running late"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton7.check(matches(isDisplayed()))

        val radioButton8 = onView(
            allOf(
                withText("Complain in a loud voice, while tapping your foot impatiently"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton8.check(matches(isDisplayed()))

        val radioButton9 = onView(
            allOf(
                withText("Complain in a loud voice, while tapping your foot impatiently"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton9.check(matches(isDisplayed()))

        val appCompatButton2 = onView(
            allOf(
                withId(R.id.next_button), withText("Next"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.cardView),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        appCompatButton2.perform(click())

        val textView5 = onView(
            allOf(
                withText("Question 3 of 14"),
                withParent(
                    allOf(
                        withId(R.id.action_bar),
                        withParent(withId(R.id.action_bar_container))
                    )
                ),
                isDisplayed()
            )
        )
        textView5.check(matches(withText("Question 3 of 14")))

        val textView6 = onView(
            allOf(
                withId(R.id.title),
                withText("You’re having an animated discussion with a colleague regarding a project that you’re in charge of. You:"),
                withParent(withParent(withId(R.id.my_nav_host_fragment))),
                isDisplayed()
            )
        )
        textView6.check(matches(withText("You’re having an animated discussion with a colleague regarding a project that you’re in charge of. You:")))

        val radioButton10 = onView(
            allOf(
                withText("Don’t dare contradict them"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton10.check(matches(isDisplayed()))

        val radioButton11 = onView(
            allOf(
                withText("Think that they are obviously right"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton11.check(matches(isDisplayed()))

        val radioButton12 = onView(
            allOf(
                withText("Defend your own point of view, tooth and nail"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton12.check(matches(isDisplayed()))

        val radioButton13 = onView(
            allOf(
                withText("Continuously interrupt your colleague"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton13.check(matches(isDisplayed()))

        val appCompatButton3 = onView(
            allOf(
                withId(R.id.next_button), withText("Next"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.cardView),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        appCompatButton3.perform(click())

        val textView7 = onView(
            allOf(
                withText("Question 4 of 14"),
                withParent(
                    allOf(
                        withId(R.id.action_bar),
                        withParent(withId(R.id.action_bar_container))
                    )
                ),
                isDisplayed()
            )
        )
        textView7.check(matches(withText("Question 4 of 14")))

        val textView8 = onView(
            allOf(
                withId(R.id.title),
                withText("You are taking part in a guided tour of a museum. You:"),
                withParent(withParent(withId(R.id.my_nav_host_fragment))),
                isDisplayed()
            )
        )
        textView8.check(matches(withText("You are taking part in a guided tour of a museum. You:")))

        val radioButton14 = onView(
            allOf(
                withText("Are a bit too far towards the back so don’t really hear what the guide is saying"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton14.check(matches(isDisplayed()))

        val radioButton15 = onView(
            allOf(
                withText("Follow the group without question"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton15.check(matches(isDisplayed()))

        val radioButton16 = onView(
            allOf(
                withText("Make sure that everyone is able to hear properly"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton16.check(matches(isDisplayed()))

        val radioButton17 = onView(
            allOf(
                withText("Are right up the front, adding your own comments in a loud voice"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton17.check(matches(isDisplayed()))

        val appCompatButton4 = onView(
            allOf(
                withId(R.id.next_button), withText("Next"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.cardView),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        appCompatButton4.perform(click())

        val textView9 = onView(
            allOf(
                withText("Question 5 of 14"),
                withParent(
                    allOf(
                        withId(R.id.action_bar),
                        withParent(withId(R.id.action_bar_container))
                    )
                ),
                isDisplayed()
            )
        )
        textView9.check(matches(withText("Question 5 of 14")))

        val textView10 = onView(
            allOf(
                withId(R.id.title),
                withText("During dinner parties at your home, you have a hard time with people who:"),
                withParent(withParent(withId(R.id.my_nav_host_fragment))),
                isDisplayed()
            )
        )
        textView10.check(matches(withText("During dinner parties at your home, you have a hard time with people who:")))

        val radioButton18 = onView(
            allOf(
                withText("Ask you to tell a story in front of everyone else"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton18.check(matches(isDisplayed()))

        val radioButton19 = onView(
            allOf(
                withText("Talk privately between themselves"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton19.check(matches(isDisplayed()))

        val radioButton20 = onView(
            allOf(
                withText("Hang around you all evening"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton20.check(matches(isDisplayed()))

        val radioButton21 = onView(
            allOf(
                withText("Always drag the conversation back to themselves"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton21.check(matches(isDisplayed()))

        val appCompatButton5 = onView(
            allOf(
                withId(R.id.next_button), withText("Next"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.cardView),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        appCompatButton5.perform(click())

        val textView11 = onView(
            allOf(
                withText("Question 6 of 14"),
                withParent(
                    allOf(
                        withId(R.id.action_bar),
                        withParent(withId(R.id.action_bar_container))
                    )
                ),
                isDisplayed()
            )
        )
        textView11.check(matches(withText("Question 6 of 14")))

        val textView12 = onView(
            allOf(
                withId(R.id.title),
                withText("You crack a joke at work, but nobody seems to have noticed. You:"),
                withParent(withParent(withId(R.id.my_nav_host_fragment))),
                isDisplayed()
            )
        )
        textView12.check(matches(withText("You crack a joke at work, but nobody seems to have noticed. You:")))

        val radioButton22 = onView(
            allOf(
                withText("Think it’s for the best — it was a lame joke anyway"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton22.check(matches(isDisplayed()))

        val radioButton23 = onView(
            allOf(
                withText("Wait to share it with your friends after work"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton23.check(matches(isDisplayed()))

        val radioButton24 = onView(
            allOf(
                withText("Try again a bit later with one of your colleagues"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton24.check(matches(isDisplayed()))

        val radioButton25 = onView(
            allOf(
                withText("Keep telling it until they pay attention"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton25.check(matches(isDisplayed()))

        val appCompatButton6 = onView(
            allOf(
                withId(R.id.next_button), withText("Next"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.cardView),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        appCompatButton6.perform(click())

        val textView13 = onView(
            allOf(
                withText("Question 7 of 14"),
                withParent(
                    allOf(
                        withId(R.id.action_bar),
                        withParent(withId(R.id.action_bar_container))
                    )
                ),
                isDisplayed()
            )
        )
        textView13.check(matches(withText("Question 7 of 14")))

        val textView14 = onView(
            allOf(
                withId(R.id.title), withText("This morning, your agenda seems to be free. You:"),
                withParent(withParent(withId(R.id.my_nav_host_fragment))),
                isDisplayed()
            )
        )
        textView14.check(matches(withText("This morning, your agenda seems to be free. You:")))

        val radioButton26 = onView(
            allOf(
                withText("Know that somebody will find a reason to come and bother you"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton26.check(matches(isDisplayed()))

        val radioButton27 = onView(
            allOf(
                withText("Heave a sigh of relief and look forward to a day without stress"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton27.check(matches(isDisplayed()))

        val radioButton28 = onView(
            allOf(
                withText("Question your colleagues about a project that’s been worrying you"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton28.check(matches(isDisplayed()))

        val radioButton29 = onView(
            allOf(
                withText("Pick up the phone and start filling up your agenda with meetings"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton29.check(matches(isDisplayed()))

        val appCompatButton7 = onView(
            allOf(
                withId(R.id.next_button), withText("Next"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.cardView),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        appCompatButton7.perform(click())

        val textView15 = onView(
            allOf(
                withText("Question 8 of 14"),
                withParent(
                    allOf(
                        withId(R.id.action_bar),
                        withParent(withId(R.id.action_bar_container))
                    )
                ),
                isDisplayed()
            )
        )
        textView15.check(matches(withText("Question 8 of 14")))

        val textView16 = onView(
            allOf(
                withId(R.id.title),
                withText("During dinner, the discussion moves to a subject about which you know nothing at all. You:"),
                withParent(withParent(withId(R.id.my_nav_host_fragment))),
                isDisplayed()
            )
        )
        textView16.check(matches(withText("During dinner, the discussion moves to a subject about which you know nothing at all. You:")))

        val radioButton30 = onView(
            allOf(
                withText("Don’t dare show that you don’t know anything about the subject"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton30.check(matches(isDisplayed()))

        val radioButton31 = onView(
            allOf(
                withText("Barely follow the discussion"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton31.check(matches(isDisplayed()))

        val radioButton32 = onView(
            allOf(
                withText("Ask lots of questions to learn more about it"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton32.check(matches(isDisplayed()))

        val radioButton33 = onView(
            allOf(
                withText("Change the subject of discussion"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton33.check(matches(isDisplayed()))

        val appCompatButton8 = onView(
            allOf(
                withId(R.id.next_button), withText("Next"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.cardView),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        appCompatButton8.perform(click())

        val textView17 = onView(
            allOf(
                withText("Question 9 of 14"),
                withParent(
                    allOf(
                        withId(R.id.action_bar),
                        withParent(withId(R.id.action_bar_container))
                    )
                ),
                isDisplayed()
            )
        )
        textView17.check(matches(withText("Question 9 of 14")))

        val textView18 = onView(
            allOf(
                withId(R.id.title),
                withText("You’re out with a group of friends and there’s a person who’s quite shy and doesn’t talk much. You:"),
                withParent(withParent(withId(R.id.my_nav_host_fragment))),
                isDisplayed()
            )
        )
        textView18.check(matches(withText("You’re out with a group of friends and there’s a person who’s quite shy and doesn’t talk much. You:")))

        val radioButton34 = onView(
            allOf(
                withText("Notice that they’re alone, but don’t go over to talk with them"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton34.check(matches(isDisplayed()))

        val radioButton35 = onView(
            allOf(
                withText("Go and have a chat with them"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton35.check(matches(isDisplayed()))

        val radioButton36 = onView(
            allOf(
                withText("Shoot some friendly smiles in their direction"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton36.check(matches(isDisplayed()))

        val radioButton37 = onView(
            allOf(
                withText("Hardly notice them at all"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton37.check(matches(isDisplayed()))

        val appCompatButton9 = onView(
            allOf(
                withId(R.id.next_button), withText("Next"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.cardView),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        appCompatButton9.perform(click())

        val textView19 = onView(
            allOf(
                withText("Question 10 of 14"),
                withParent(
                    allOf(
                        withId(R.id.action_bar),
                        withParent(withId(R.id.action_bar_container))
                    )
                ),
                isDisplayed()
            )
        )
        textView19.check(matches(withText("Question 10 of 14")))

        val textView20 = onView(
            allOf(
                withId(R.id.title),
                withText("At work, somebody asks for your help for the hundredth time. You:"),
                withParent(withParent(withId(R.id.my_nav_host_fragment))),
                isDisplayed()
            )
        )
        textView20.check(matches(withText("At work, somebody asks for your help for the hundredth time. You:")))

        val radioButton38 = onView(
            allOf(
                withText("Give them a hand, as usual"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton38.check(matches(isDisplayed()))

        val radioButton39 = onView(
            allOf(
                withText("Accept — you’re known for being helpful"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton39.check(matches(isDisplayed()))

        val radioButton40 = onView(
            allOf(
                withText("Ask them, please, to find somebody else for a change"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton40.check(matches(isDisplayed()))

        val radioButton41 = onView(
            allOf(
                withText("Loudly make it known that you’re annoyed"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton41.check(matches(isDisplayed()))

        val appCompatButton10 = onView(
            allOf(
                withId(R.id.next_button), withText("Next"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.cardView),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        appCompatButton10.perform(click())

        val textView21 = onView(
            allOf(
                withText("Question 11 of 14"),
                withParent(
                    allOf(
                        withId(R.id.action_bar),
                        withParent(withId(R.id.action_bar_container))
                    )
                ),
                isDisplayed()
            )
        )
        textView21.check(matches(withText("Question 11 of 14")))

        val textView22 = onView(
            allOf(
                withId(R.id.title),
                withText("You’ve been see a movie with your family and the reviews are mixed. You:"),
                withParent(withParent(withId(R.id.my_nav_host_fragment))),
                isDisplayed()
            )
        )
        textView22.check(matches(withText("You’ve been see a movie with your family and the reviews are mixed. You:")))

        val radioButton42 = onView(
            allOf(
                withText("Don’t share your point of view with anyone"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton42.check(matches(isDisplayed()))

        val radioButton43 = onView(
            allOf(
                withText("Didn’t like the film, but keep your views to yourself when asked"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton43.check(matches(isDisplayed()))

        val radioButton44 = onView(
            allOf(
                withText("State your point of view with enthusiasm"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton44.check(matches(isDisplayed()))

        val radioButton45 = onView(
            allOf(
                withText("Try to bring the others round to your point of view"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton45.check(matches(isDisplayed()))

        val appCompatButton11 = onView(
            allOf(
                withId(R.id.next_button), withText("Next"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.cardView),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        appCompatButton11.perform(click())

        val textView23 = onView(
            allOf(
                withText("Question 12 of 14"),
                withParent(
                    allOf(
                        withId(R.id.action_bar),
                        withParent(withId(R.id.action_bar_container))
                    )
                ),
                isDisplayed()
            )
        )
        textView23.check(matches(withText("Question 12 of 14")))

        val textView24 = onView(
            allOf(
                withId(R.id.title), withText("A friend arrives late for your meeting. You:"),
                withParent(withParent(withId(R.id.my_nav_host_fragment))),
                isDisplayed()
            )
        )
        textView24.check(matches(withText("A friend arrives late for your meeting. You:")))

        val radioButton46 = onView(
            allOf(
                withText("Say, ‘It’s not a problem,’ even if that’s not what you really think"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton46.check(matches(isDisplayed()))

        val radioButton47 = onView(
            allOf(
                withText("Give them a filthy look and sulk for the rest of the evening"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton47.check(matches(isDisplayed()))

        val radioButton48 = onView(
            allOf(
                withText("Tell them, ‘You’re too much! Have you seen the time?’"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton48.check(matches(isDisplayed()))

        val radioButton49 = onView(
            allOf(
                withText("Make a scene in front of everyone"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton49.check(matches(isDisplayed()))

        val appCompatButton12 = onView(
            allOf(
                withId(R.id.next_button), withText("Next"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.cardView),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        appCompatButton12.perform(click())

        val textView25 = onView(
            allOf(
                withText("Question 13 of 14"),
                withParent(
                    allOf(
                        withId(R.id.action_bar),
                        withParent(withId(R.id.action_bar_container))
                    )
                ),
                isDisplayed()
            )
        )
        textView25.check(matches(withText("Question 13 of 14")))

        val textView26 = onView(
            allOf(
                withId(R.id.title), withText("You can’t find your car keys. You:"),
                withParent(withParent(withId(R.id.my_nav_host_fragment))),
                isDisplayed()
            )
        )
        textView26.check(matches(withText("You can’t find your car keys. You:")))

        val radioButton50 = onView(
            allOf(
                withText("Don’t want anyone to find out, so you take the bus instead"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton50.check(matches(isDisplayed()))

        val radioButton51 = onView(
            allOf(
                withText("Panic and search madly without asking anyone for help"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton51.check(matches(isDisplayed()))

        val radioButton52 = onView(
            allOf(
                withText("Grumble without telling your family why you’re in a bad mood"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton52.check(matches(isDisplayed()))

        val radioButton53 = onView(
            allOf(
                withText("Accuse those around you for misplacing them"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton53.check(matches(isDisplayed()))

        val appCompatButton13 = onView(
            allOf(
                withId(R.id.next_button), withText("Next"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.cardView),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        appCompatButton13.perform(click())

        val textView27 = onView(
            allOf(
                withText("Question 14 of 14"),
                withParent(
                    allOf(
                        withId(R.id.action_bar),
                        withParent(withId(R.id.action_bar_container))
                    )
                ),
                isDisplayed()
            )
        )
        textView27.check(matches(withText("Question 14 of 14")))

        val textView28 = onView(
            allOf(
                withId(R.id.title),
                withText("It’s time for your annual appraisal with your boss. You:"),
                withParent(withParent(withId(R.id.my_nav_host_fragment))),
                isDisplayed()
            )
        )
        textView28.check(matches(withText("It’s time for your annual appraisal with your boss. You:")))

        val radioButton54 = onView(
            allOf(
                withText("Go with great hesitation as these sessions are torture for you"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton54.check(matches(isDisplayed()))

        val radioButton55 = onView(
            allOf(
                withText("Look forward to hearing what your boss thinks about you and expects from you"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton55.check(matches(isDisplayed()))

        val radioButton56 = onView(
            allOf(
                withText("Rehearse ad nauseam the arguments and ideas that you’ve prepared for the meeting"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton56.check(matches(isDisplayed()))

        val radioButton57 = onView(
            allOf(
                withText("Go along unprepared as you are confident and like improvising"),
                withParent(
                    allOf(
                        withId(R.id.options),
                        withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        radioButton57.check(matches(isDisplayed()))

        val button5 = onView(
            allOf(
                withId(R.id.next_button), withText("FINISH"),
                withParent(withParent(withId(R.id.cardView))),
                isDisplayed()
            )
        )
        button5.check(matches(isDisplayed()))

        val appCompatButton14 = onView(
            allOf(
                withId(R.id.next_button), withText("Finish"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.cardView),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        appCompatButton14.perform(click())

        val textView29 = onView(
            allOf(
                withText("Result"),
                withParent(
                    allOf(
                        withId(R.id.action_bar),
                        withParent(withId(R.id.action_bar_container))
                    )
                ),
                isDisplayed()
            )
        )
        textView29.check(matches(withText("Result")))

        val textView30 = onView(
            allOf(
                withId(R.id.title), withText("You are more of an extrovert"),
                withParent(withParent(IsInstanceOf.instanceOf(NestedScrollView::class.java))),
                isDisplayed()
            )
        )
        textView30.check(matches(withText("You are more of an extrovert")))

        val textView31 = onView(
            allOf(
                withId(R.id.description), withText(
                    "Whether at work or at home, you are a leader, the head of the pack. You are the type of person who is at ease with everyone — with the grocer, the doctor, a managing director or a waiter. You have an opinion about just about everything and you like to share your knowledge around, even imposing it on others if they haven’t asked for it. Your personal and professional entourages easily class you as a ‘loud mouth’, sure about yourself, not in the least bit bothered about what others think of you and someone who occasionally likes to play the card of provocation. When you’re on a roll, it’s hard to sop you and the least that could be said is that listening skills are not one of your innate skills. In the couples arena, you have maybe fallen for someone with a similar temperament – making for animated evenings! Or on the contrary, you live with a more introverted partner over whom you can, in some cases, have the upper hand. Your confidence and exuberance are a strong motor for your highly colourful life and you are a real antidote to any hint of grey skies or boredom. It’s perhaps the moments of excess activity or confidence that you need to question. Your capacity to be over the top can make others feel that you consider yourself as the most important person, that you have to be the centre of attention to feel loved and thus to exist, unless of course these are ways of pre-empting judgement from others. Do you worry about leaving others indifferent if you’re not perpetually ‘on show’? Maybe this need for attention is to compensate your own self-judgement? Your energy levels and enthusiasm are unquestionable, but are you as comfortable receiving as giving? Have you actually listened to anyone recently, without butting in? Do you know how to stay by yourself and do nothing else but be with yourself? A few questions that could be interesting to ask yourself…"
                ),
                withParent(withParent(IsInstanceOf.instanceOf(NestedScrollView::class.java))),
                isDisplayed()
            )
        )
        textView31.check(
            matches(
                withText(
                    "Whether at work or at home, you are a leader, the head of the pack. You are the type of person who is at ease with everyone — with the grocer, the doctor, a managing director or a waiter. You have an opinion about just about everything and you like to share your knowledge around, even imposing it on others if they haven’t asked for it. Your personal and professional entourages easily class you as a ‘loud mouth’, sure about yourself, not in the least bit bothered about what others think of you and someone who occasionally likes to play the card of provocation. When you’re on a roll, it’s hard to sop you and the least that could be said is that listening skills are not one of your innate skills. In the couples arena, you have maybe fallen for someone with a similar temperament – making for animated evenings! Or on the contrary, you live with a more introverted partner over whom you can, in some cases, have the upper hand. Your confidence and exuberance are a strong motor for your highly colourful life and you are a real antidote to any hint of grey skies or boredom. It’s perhaps the moments of excess activity or confidence that you need to question. Your capacity to be over the top can make others feel that you consider yourself as the most important person, that you have to be the centre of attention to feel loved and thus to exist, unless of course these are ways of pre-empting judgement from others. Do you worry about leaving others indifferent if you’re not perpetually ‘on show’? Maybe this need for attention is to compensate your own self-judgement? Your energy levels and enthusiasm are unquestionable, but are you as comfortable receiving as giving? Have you actually listened to anyone recently, without butting in? Do you know how to stay by yourself and do nothing else but be with yourself? A few questions that could be interesting to ask yourself…"
                )
            )
        )
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
